package com.d.demo;

import java.util.ArrayList;

/**
 * Created by d on 2018/7/14.
 */
public abstract class Pizza {

    String name;  // 名称
    String dough; // 面团
    String sauce; // 酱
    ArrayList toppings = new ArrayList();

    void prepare() {
        System.out.println("Preparing" + name);
        System.out.println("Tossing dough...");
        System.out.println("Adding sauce...");
        System.out.println("Adding toppings:");
        for (int i = 0; i < toppings.size(); i++) {
            System.out.println("    " + toppings.get(i));
        }
    }

    void bake() {
        System.out.println("Bake for 25 minutes at 350");
    }

    void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }

}
