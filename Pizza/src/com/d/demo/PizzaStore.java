package com.d.demo;

/**
 * Created by d on 2018/7/14.
 */
public abstract class PizzaStore {

    public Pizza orderPrzza(String type) {

        Pizza pizza;

        pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;

    }

    protected abstract Pizza createPizza(String type);

}
