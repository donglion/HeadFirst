package com.d.demo;

/**
 * Created by d on 2018/7/14.
 */
public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chicagoStore = new ChicagoPizzaStore();

        Pizza pizza = nyStore.createPizza("cheese");
        System.out.println(pizza.getName());
        pizza = chicagoStore.createPizza("cheese");
        System.out.println(pizza.getName());
        nyStore.orderPrzza("cheese");
    }

}
