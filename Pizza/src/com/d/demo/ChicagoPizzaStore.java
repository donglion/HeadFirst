package com.d.demo;

/**
 * Created by d on 2018/7/14.
 */
public class ChicagoPizzaStore extends PizzaStore {

    protected Pizza createPizza(String item) {
        if (item.equals("cheese")) {
            return new ChicagoStyleCheesePizza();
        }
        return null;
    }

}
