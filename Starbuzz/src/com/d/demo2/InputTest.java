package com.d.demo2;

import java.io.*;

/**
 * Created by d on 2018/7/14.
 */
public class InputTest {

    public static void main(String[] args) {
        int c;
        try {
            InputStream in = new LowerCaseInputStream(new BufferedInputStream(new FileInputStream("D:\\Coding\\HeadFirst\\Starbuzz\\src\\com\\d\\demo2\\test.txt")));
            while ((c = in.read()) >= 0) {
                System.out.print((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
