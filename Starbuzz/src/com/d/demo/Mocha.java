package com.d.demo;

/**
 * 摩卡
 * Created by d on 2018/7/14.
 */
public class Mocha extends CondimentDecorator {

    Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", 摩卡";
    }

    @Override
    public double cost() {
        return 0.20 + beverage.cost();
    }

}
