package com.d.demo;

/**
 * 饮料
 * Created by d on 2018/7/14.
 */
public abstract class Beverage {

    String description = "Unknow Beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();

}
