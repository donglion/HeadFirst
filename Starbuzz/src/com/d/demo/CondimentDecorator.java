package com.d.demo;

/**
 * 调味
 * Created by d on 2018/7/14.
 */
public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();

}
