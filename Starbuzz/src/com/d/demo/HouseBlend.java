package com.d.demo;

/**
 * 家庭混合
 * Created by d on 2018/7/14.
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "家庭混合咖啡";
    }

    @Override
    public double cost() {
        return 0.89;
    }
    
}
