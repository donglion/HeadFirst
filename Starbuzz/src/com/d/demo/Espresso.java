package com.d.demo;

/**
 * 意大利浓咖啡
 * Created by d on 2018/7/14.
 */
public class Espresso extends Beverage {

    public Espresso() {
        description = "意大利浓咖啡";
    }

    @Override
    public double cost() {
        return 1.99;
    }

}
