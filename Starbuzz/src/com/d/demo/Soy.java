package com.d.demo;

/**
 * 酱油
 * Created by d on 2018/7/14.
 */
public class Soy extends CondimentDecorator {

    Beverage beverage;

    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", 酱油";
    }

    @Override
    public double cost() {
        return 0.30 + beverage.cost();
    }

}
