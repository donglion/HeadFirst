package com.d.demo;

/**
 * 暗烤咖啡
 * Created by d on 2018/7/14.
 */
public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "暗烤咖啡";
    }

    @Override
    public double cost() {
        return 2.99;
    }

}
