package com.d.base;

import com.d.base.FlyBehavior;

/**
 * Created by d on 2018/7/8.
 */
public class FlyNoWay implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("什么都不做，不会飞");
    }

}
