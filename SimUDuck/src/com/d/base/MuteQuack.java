package com.d.base;

import com.d.base.QuackBehavior;

/**
 * Created by d on 2018/7/8.
 */
public class MuteQuack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("不会叫");
    }
}
