package com.d.ducks;

import com.d.base.Duck;

/**
 * 诱饵鸭
 * Created by d on 2018/7/8.
 */
public class DecoyDuck extends Duck {

    @Override
    public void display() {
        System.out.println("诱饵鸭");
    }

}
