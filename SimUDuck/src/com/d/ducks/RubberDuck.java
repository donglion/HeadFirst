package com.d.ducks;

import com.d.base.Duck;

/**
 * Created by d on 2018/7/8.
 */
public class RubberDuck extends Duck {

    @Override
    public void display() {
        System.out.println("橡皮鸭");
    }

}
