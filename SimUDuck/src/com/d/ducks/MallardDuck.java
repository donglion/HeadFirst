package com.d.ducks;

import com.d.base.Duck;
import com.d.base.FlyWithWings;
import com.d.base.Quack;

/**
 * Created by d on 2018/7/8.
 */
public class MallardDuck extends Duck {

    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }


    @Override
    public void display() {
        System.out.println("绿头鸭");
    }

}
