package com.d.ducks;

import com.d.base.Duck;
import com.d.base.FlyNoWay;
import com.d.base.Quack;

/**
 * Created by d on 2018/7/8.
 */
public class ModelDuck extends Duck {

    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("模型鸭");
    }

}
