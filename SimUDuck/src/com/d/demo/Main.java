package com.d.demo;

import com.d.base.Duck;
import com.d.base.FlyRocketPowered;
import com.d.ducks.MallardDuck;

/**
 * Created by d on 2018/7/8.
 */
public class Main {

    public static void main(String[] args) {
        Duck duck = new MallardDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();
        duck.swim();
        duck.setFlyBehavior(new FlyRocketPowered());
        duck.performFly();
    }

}
