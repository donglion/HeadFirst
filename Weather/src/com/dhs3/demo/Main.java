package com.dhs3.demo;

import com.dhs3.ann.CheckDelete;
import com.dhs3.ann.CheckField;
import com.dhs3.ann.Table;
import com.dhs3.entity.Category;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Created by d on 2018/7/9.
 */
public class Main {

    public static void main(String[] args) {

        Category category = new Category();
        deleted(category);

    }

    public static boolean deleted(Object object) {

        Class<?> clazz = object.getClass();

        boolean hasCheckAnn = clazz.isAnnotationPresent(CheckDelete.class);
        boolean hasTable = clazz.isAnnotationPresent(Table.class);
        System.out.println("hasCheckAnn:" + hasCheckAnn);
        System.out.println("hasTable:" + hasTable);
        Annotation[] declaredAnnotations = clazz.getDeclaredAnnotations();
        System.out.println("declaredAnnotations:" + declaredAnnotations.length);
        if (hasCheckAnn) {
            CheckDelete checkDelete = clazz.getAnnotation(CheckDelete.class);
            Class[] tables = checkDelete.tables();
            System.out.println("tables:" + tables.length);
            for (int i = 0; i < tables.length; i++) {
                Class table = tables[i];
                boolean isTable = table.isAnnotationPresent(Table.class);
                if (isTable) {
                    Field[] fields = table.getDeclaredFields();
                    for (int j = 0; j < fields.length; j++) {
                        boolean annotationPresent = fields[j].isAnnotationPresent(CheckField.class);
                        if (annotationPresent) {
                            System.out.println("hasCheckAnn:" + table.getSimpleName() + "表,有" + fields[j].getName() + ":" + annotationPresent);

                        }
                    }
                }
            }
        }

        return true;
    }

}
