package com.dhs3.entity;

import com.dhs3.ann.CheckDelete;
import com.dhs3.ann.CheckField;
import com.dhs3.ann.Table;

import java.io.Serializable;

@CheckDelete(tables = {Items.class, Goods.class})
@Table(name = "category")
public class Category implements Serializable {

    private Long id;

    @CheckField
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}