package com.dhs2;

/**
 * Created by d on 2018/3/10.
 */
public interface DisplayElement {

    void display();

}
