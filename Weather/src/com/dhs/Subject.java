package com.dhs;

/**
 * Created by d on 2018/3/10.
 */
public interface Subject {

    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObserver();

}
