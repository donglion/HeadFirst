package com.dhs;

/**
 * Created by d on 2018/3/10.
 */
public interface Observer {

    void update(float temperature, float humidity, float pressure);

}
