package com.dhs;

/**
 * Created by d on 2018/3/10.
 */
public interface DisplayElement {

    void display();

}
